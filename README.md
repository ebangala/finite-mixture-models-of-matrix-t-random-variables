# Finite Mixture Models Of Matrix-t Random Variables

Development of an EM Algorithm for parameter estimation of a finite mixture model of matrix-t variables & implementation in R using simulated data